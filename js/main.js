
// SVG IE11 support
svg4everybody();

// Mobile Navigation
$('.header__toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.page').toggleClass('open_nav');
});


$(".btn_modal").fancybox({
    'padding'    : 0
});

$('.header__lng_active').on('click touchstart', function(e) {
    e.preventDefault();

    if($(this).closest('.header__lng').hasClass('open')){
        $(this).closest('.header__lng').toggleClass('open');
    }
    else {
        $('.header__lng').removeClass('open');
        $(this).closest('.header__lng').toggleClass('open');
    }
});

$('body').click(function (event) {

    if ($(event.target).closest(".header__lng").length === 0) {
        $(".header__lng").removeClass('open');
    }

});

// Mobile Navigation
$('.header__toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.page').toggleClass('open_nav');
});

$('.timer').countdown_sg(28);


$('.btn_send').click(function() {

    $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
    var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
    if(answer != false)
    {
        var $form = $(this).closest('form'),
            name = $('input[name="name"]', $form).val(),
            email = $('input[name="email"]', $form).val(),
            message  = $('textarea[name="message"]', $form).val();

        $.ajax({
            type: "POST",
            url: "form-handler.php",
            data: {email:email, name:name, message:message}
        }).done(function(msg) {
            $('form').find('input[type=text], textarea').val('');
            console.log('удачно');
            $('.btn_thanks').trigger('click');
            setTimeout('$.fancybox.close();',5000);
        });
    }
});
